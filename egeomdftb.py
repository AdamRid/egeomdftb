#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Script para ejecucción automatizada del dftb+ a partir de un fichero 
'geom.out.xyz' y 'dftb_in.hsd'
Testeado sobre python 2.6.6
version 0.2.0
"""

import os
import sys
import re
import time
import subprocess
import logging


# Ruta absoluta al proyecto
ROOT_DIR = os.path.dirname(os.path.abspath(__file__))

logger = logging.getLogger()
logger.setLevel(logging.INFO)
_formatter = logging.Formatter(fmt='%(asctime)s: %(message)s', datefmt='%m/%d/%Y %H:%M:%S')

# Creamos un handler de consola y le asignamos el nivel INFO
_output_ch = "%s/egeomdftb.log" % ROOT_DIR
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
ch.setFormatter(_formatter)
logger.addHandler(ch)

# Creamos un handler de fichero y le asignamos el nivel DEBUG
_output_fh = "%s/egeomdftb.log" % ROOT_DIR
fh = logging.FileHandler(_output_fh)
fh.setLevel(logging.DEBUG)
fh.setFormatter(_formatter)
logger.addHandler(fh)


class GeomInputFile:
    """Clase que define un objeto que representa el fichero de geometrías.
     Attributos:
        file_path:      Ruta al archivo de las geometrías
        iters_dict:     Contenedor que almacena la posición y número de
                        linea de cada iteración definida en el fichero
                        de geometrías
        iters_num:      Número de iteraciones totales
    """

    def __init__(self, file_path):
        self.file_path = file_path
        self.iters_dict = {}
        self.iters_num = self._number_of_iteration()
        if self.iters_num == 0:
            raise SyntaxError("Error: ninguna iteración encontrada en el fihero '%s'."
                              "\n\tAsegurate de que el fichero de las geometrias cumpla con el formato." % file_path)

    def get_iteration_data(self, iteration_num):
        """Método que devuelve las geometrías correspondientes a una iteración X

        :param iteration_num:   número de iteración de la cuál se solicitan las geometrías
        :return:    diccionario contenedor de:
                        types:      lista de los tipos de átomos de las geometrías
                        positions:  lista de las posiciones de cada átomo
        """
        if not os.path.isfile(self.file_path):
            raise IOError("Error: fichero de las geometrias '%s' no encontrado" % self.file_path)

        if iteration_num not in self.iters_dict:
            raise KeyError("Error: la iteracion '%d' no existe en el fichero de las geometrias" % iteration_num)

        f = open(self.file_path, "r")
        f.seek(self.iters_dict[iteration_num][0])
        bad_char = re.compile("[\n ]")
        total_atoms = int(bad_char.sub("", f.readline()))
        f.readline()  # Saltamos la cabecera "MD iter: X"
        data = {
            "types": list(),
            "positions": list(),
        }
        for i in range(total_atoms):
            line = f.readline()
            line = re.sub("^[ ]{1,}|\n", "", line)
            line = re.sub("[ ]{1,}", ";", line)
            atom_data = line.split(";")[:8]
            data["positions"].append(atom_data)
            if atom_data[0] not in data["types"]:
                data["types"].append(atom_data[0])
        f.close()
        return data

    def _number_of_iteration(self):
        if not os.path.isfile(self.file_path):
            raise IOError("Error: fichero de las geometrias '%s' no encontrado" % self.file_path)

        f = open(self.file_path, "r")
        header_pattern = re.compile(r"^[ ]+[0-9]+\n")
        total_iters = pos = 0
        current_line = 1
        for line in iter(f.readline, ''):
            if header_pattern.search(line):
                self.iters_dict[total_iters] = (pos, current_line)
                total_iters += 1
            pos = f.tell()
            current_line += 1
        f.close()
        return total_iters


class DftbInputFile:
    """Clase que define un objeto que representa el fichero input del
    programa dftb+
    Atributos:
        model_file_path:        ruta al archivo input que se copia
        model_file:             fichero input actual construido a partir del
                                modelo indicado en el constructor
    """
    atoms_types_pattern = re.compile(r'TypeNames = {[ "a-zA-Z]*?}')
    coordinates_pattern = re.compile(r"TypesAndCoordinates \[Angstrom\] = {(?:.|\n)*?}")
    steps_pattern = re.compile(r"Steps = [0-9]+")
    velocities_pattern = re.compile(r"ConvergentForcesOnly = No")
    skf_pattern = re.compile(r"SlaterKosterFiles {(?:.|\n)*?}")
    mam_pattern = re.compile(r"MaxAngularMomentum {(?:.|\n)*?}")

    def __init__(self, model_file_path):
        self.model_file_path = model_file_path
        self.model_file = self._load_model_file()
        self._check_model_format()
        self._clear_model_file()
        self.current_atom_types = []
        self.sk_files = self._get_re_matching(self.skf_pattern)
        self.mam = self._get_re_matching(self.mam_pattern)

    def get_dftb_in(self):
        # Limpiamos saltos de linea entre lineas vacias
        self.model_file = re.sub(r"(?:\n(?: |\r)+\n)+", "\n", self.model_file)
        # Añadimos un salto de linea entre bloques de primer nivel
        self.model_file = re.sub(r"\n}\r?\n(?=.)", "\n}\n\n", self.model_file)
        return self.model_file

    def set_geometries(self, geometries):
        self._clear_model_file()
        self._set_atoms_types(geometries["positions"])
        self._set_positions(geometries["types"], geometries["positions"])
        self._set_sk_files()
        self._set_mam()
    
    def set_velocities(self, velocities):
        # Borrado de las antiguas velocidades
        self.model_file = re.sub(r"\n?Velocities \[AA/ps\] = {(?:.|\n)*?}", "", self.model_file)
        # Adición de las nuevas velocidades
        _str = ""
        for velocity in velocities:
            _str += "  %s    %s    %s\n" % (velocity[5], velocity[6], velocity[7])
        self.model_file = re.sub(self.velocities_pattern,
                                 "ConvergentForcesOnly = No\n  Velocities [AA/ps] = {\n%s  }" % _str,
                                 self.model_file)

    def _get_re_matching(self, reg):
        """Devuelve los datos que cumplen la expresión regular indicada
        en el parámetro 'reg'"""
        if re.search(reg, self.model_file):
            # solo se devuelve la primera ocurrencia
            re_data = re.findall(reg, self.model_file)[0]
            return re_data
        else:
            return ""
    
    def _set_sk_files(self):
        """Se añaden únicamente los fichero SlaterKosterFiles de los átomos con los
        que se esta trabajando. Se presupone que el dftb_in.hsd.model contiene todas
        las referencias a los ficheros SlaterKosterFiles de cada átomo."""
        sk_files_update = []
        for i in range(0, len(self.current_atom_types)):
            atom_a = self.current_atom_types[i]
            for j in range(i, len(self.current_atom_types)):
                atom_b = self.current_atom_types[j]
                rga = re.compile(r"%(a)s-%(b)s|%(b)s-%(a)s" % {"a": atom_a, "b": atom_b})
                for line in self.sk_files.split("\n"):
                    if re.search(rga, line):
                        sk_files_update.append(line)
        if sk_files_update:
            self.model_file = re.sub(self.skf_pattern, "SlaterKosterFiles {\n%s\n  }" %
                                     "\n".join(sk_files_update), self.model_file)

    def _set_mam(self):
        """Actulizamos el campo MaxAngularMomentum únicamente con aquellos atomos
        con los que estemos trabajando"""
        mam_update = []
        for atom in self.current_atom_types:
            rga = re.compile("%s =" % atom)
            for line in self.mam.split("\n"):
                if re.search(rga, line):
                    mam_update.append(line)
        if mam_update:
            self.model_file = re.sub(self.mam_pattern, "MaxAngularMomentum {\n%s\n  }" %
                                     "\n".join(mam_update), self.model_file)

    def _set_atoms_types(self, positions):
        atom_types = []
        for position in positions:
            if position[0] not in atom_types:
                atom_types.append(position[0])
        _str = '"' + '" "'.join(atom_types) + '"'
        self.model_file = re.sub(self.atoms_types_pattern, "TypeNames = {%s}" % _str, self.model_file)
        self.current_atom_types = atom_types

    def _set_positions(self, atom_types, positions):
        _str = ""
        for position in positions:
            _str += "  %d    %s    %s    %s\n" % \
                    (self.current_atom_types.index(position[0]) + 1, position[1], position[2], position[3])
        self.model_file = re.sub(self.coordinates_pattern, "TypesAndCoordinates [Angstrom] = {\n%s  }" % _str,
                                 self.model_file)

    def _load_model_file(self):
        if not os.path.isfile(self.model_file_path):
            raise IOError("Error: el fichero modelo del dftb_in '%s' no encontrado" % self.model_file_path)

        f = open(self.model_file_path)
        content = f.read()
        f.close()
        return content

    def _check_model_format(self):
        if not re.search(self.atoms_types_pattern, self.model_file) or \
                not re.search(self.coordinates_pattern, self.model_file) or \
                not re.search(self.steps_pattern, self.model_file):
            raise SyntaxError("Error: el fichero modelo del dftb_in '%s' no parece cumplir con el formato." %
                              self.model_file_path)

    def _clear_model_file(self):
        self.model_file = re.sub(self.atoms_types_pattern, "TypeNames = {}", self.model_file)
        self.model_file = re.sub(self.coordinates_pattern, "TypesAndCoordinates [Angstrom] = {}", self.model_file)
        self.model_file = re.sub(self.steps_pattern, "Steps = 1", self.model_file)


def create_dftb_file(dftb_in):
    file_name = "dftb_in.hsd"
    f = open(file_name, "w")
    f.write(dftb_in)
    f.close()
    return file_name


def exec_dftb(dftb_file):
    out = subprocess.Popen(['dftb+', dftb_file],
                           stdout=subprocess.PIPE,
                           stderr=subprocess.STDOUT)
    stdout, stderr = out.communicate()
    temperature_pattern = re.compile(r"MD Temperature::[- .HK0-9]+")
    energy_pattern = re.compile(r"Total MD Energy:[- .HeV0-9]+")
    energy = temperature = ""
    if stderr is not None:
        logging.error("DFTB+ ERROR: \n%s" % stderr)
    print "DFTB+ OUTPUT:\n%s" % stdout
    if re.search(temperature_pattern, stdout):
        temperature = re.search(temperature_pattern, stdout).group()
    if re.search(energy_pattern, stdout):
        energy = re.search(energy_pattern, stdout).group()

    return stdout, temperature, energy


def runner(geometries_file, dftb_in_file, offset_=1, first_atom=1, last_atom=0):
    start_time = time.time()
    print "Cargando fichero de las geometrias"
    geomif = GeomInputFile(geometries_file)
    print "Cargando fichero dftb_in"
    dftbif = DftbInputFile(dftb_in_file)
    for i in range(0, geomif.iters_num, offset_):
        print "Ejecutando iteracion %d" % i
        iteration_data = geomif.get_iteration_data(i)
        if last_atom <= len(iteration_data["positions"]) and last_atom != 0:
            iteration_data["positions"] = iteration_data["positions"][first_atom - 1:last_atom]
        print "Numero de atomos: %d" % len(iteration_data["positions"])
        dftbif.set_geometries(iteration_data)
        dftbif.set_velocities(iteration_data["positions"])
        dftb_in = dftbif.get_dftb_in()
        dftb_file = create_dftb_file(dftb_in)
        stdout, temperature, energy = exec_dftb(dftb_file)
        print "\nResultado iteracion %d" % i
        print "\t%s" % temperature
        print "\t%s" % energy

    elapsed_time = time.time() - start_time
    print "Ejecucion terminada.\nTiempo transcurrido: %fs" % elapsed_time


def print_run_example():
    print 'Ejemplo:\n\tpython %s geom.out.xyz dftb_in.hsd ' \
          '[start=num_atom end=num_atom] [offset=num_offset]' % os.path.basename(__file__)


if __name__ == "__main__":
    if len(sys.argv) < 3:
        print "Argumentos insuficientes"
        print_run_example()
        exit(1)
    offset = 1
    first_atom = 0
    last_atom = 0
    if len(sys.argv) != 3:
        # Si existen argumentos adicionales
        args = sys.argv[3:]
        keys_list = list()
        for arg in args:
            try:
                k = arg.split("=")[0]
                v = arg.split("=")[1]
            except IndexError:
                print "Argumento '%s' no reconocido" % arg
                print_run_example()
                exit(2)
            try:
                if k == "offset" and int(v) != 0:
                    offset = int(v)
            except ValueError:
                print "Error en el valor del offset"
                exit(3)
            try:
                if k == "start":
                    first_atom = int(v)
            except ValueError:
                print "Error en el valor de la posicion del primer atomo"
                exit(4)
            try:
                if k == "end":
                    last_atom = int(v)
            except ValueError:
                print "Error en el valor de la posicion del ultimo atomo"
                exit(5)
            keys_list.append(k)

        if "start" in keys_list and "end" not in keys_list:
            print "Error en los argumentos. No se ha indicado la posicion del ultimo atomo"
            exit(6)
        elif "end" in keys_list and "start" not in keys_list:
            print "Error en los argumentos. No se ha indicado la posicion del primer atomo"
            exit(7)
        if first_atom > last_atom:
            print "La posicion del primer atomo ha de ser menor al del ultimo atomo"
            exit(8)
    try:
        runner(sys.argv[1], sys.argv[2], offset, first_atom, last_atom)
    except Exception as e:
        print "Abortando ejecucion."
        print "\t%s" % e
        exit(9)